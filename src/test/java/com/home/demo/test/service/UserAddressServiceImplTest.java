package com.home.demo.test.service;

import com.home.demo.dto.ResponseBody;
import com.home.demo.dto.UserAddressRequest;
import com.home.demo.entity.UserAddress;
import com.home.demo.repository.UserAddressRepository;
import com.home.demo.service.impl.UserAddressServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class UserAddressServiceImplTest {

    private static final String TEST = "test";
    private static final String BOTH = "Both";
    private static final String PRIMARY = "Primary";
    private static final int LIMIT = 10;
    private static final int ADDRESS_TYPE_BOTH = 0;
    private static final int ADDRESS_TYPE_PRIMARY = 1;

    @InjectMocks
    UserAddressServiceImpl userAddressServiceImpl;

    @Mock
    UserAddressRepository userAddressRepository;

    @Mock
    private Page<UserAddress> userAddressPage;

    @Test
    public void checkAddress(){
        when(userAddressRepository.findAll(any(Pageable.class))).thenReturn(userAddressPage);
        UserAddressRequest userAddressRequest = createUserAddressRequest();
        ResponseEntity responseEntity = userAddressServiceImpl.getUserAdress(userAddressRequest);
        assertThat(responseEntity).extracting(ResponseEntity::getStatusCodeValue).isEqualTo(200);
    }

    @Test
    public void checkAddressWithContent(){
        when(userAddressRepository.getByAddressOrPincodeOrCountry(anyString(), any(Pageable.class))).thenReturn(userAddressPage);
        when(userAddressPage.getContent()).thenReturn(new ArrayList<UserAddress>(){{add(new UserAddress(){{setAddress("Test");}});}});
        UserAddressRequest userAddressRequest = createUserAddressRequest();
        userAddressRequest.setTextInput(TEST);
        userAddressRequest.setAddressType(null);
        ResponseEntity responseEntity = userAddressServiceImpl.getUserAdress(userAddressRequest);
        assertTrue(((ResponseBody)responseEntity.getBody()).getAddressList().get(ADDRESS_TYPE_BOTH).getAddress().contentEquals("Test"));
    }

    @Test
    public void checkPrimaryOrSecondaryAddressWithNoContent(){
        when(userAddressRepository.findByAddressType(anyInt(), any(Pageable.class))).thenReturn(userAddressPage);
        when(userAddressPage.getContent()).thenReturn(new ArrayList<UserAddress>(){{add(new UserAddress(){{setAddressType(ADDRESS_TYPE_PRIMARY);}});}});
        UserAddressRequest userAddressRequest = createUserAddressRequest();
        userAddressRequest.setAddressType(ADDRESS_TYPE_PRIMARY);
        ResponseEntity responseEntity = userAddressServiceImpl.getUserAdress(userAddressRequest);
        assertTrue(((ResponseBody)responseEntity.getBody()).getAddressList().get(ADDRESS_TYPE_BOTH).getAddressType().contentEquals(PRIMARY));
    }

    @Test
    public void checkPrimaryOrSecondaryAddressWithContent(){
        when(userAddressRepository.getByAddressOrPincodeOrCountryAndAddressType(anyString(), anyInt(), any(Pageable.class))).thenReturn(userAddressPage);
        when(userAddressPage.getContent()).thenReturn(new ArrayList<UserAddress>(){{add(new UserAddress(){{setAddressType(ADDRESS_TYPE_PRIMARY); setAddress(TEST);}});}});
        ResponseEntity responseEntity = userAddressServiceImpl.getUserAdress(new UserAddressRequest() {{setLimit(LIMIT); setAddressType(ADDRESS_TYPE_PRIMARY); setTextInput(TEST);}});
        assertTrue(((ResponseBody)responseEntity.getBody()).getAddressList().get(ADDRESS_TYPE_BOTH).getAddressType().contentEquals(PRIMARY));
        assertTrue(((ResponseBody)responseEntity.getBody()).getAddressList().get(ADDRESS_TYPE_BOTH).getAddress().contentEquals(TEST));
    }

    @Test
    public void checkBothAddressWithContent(){
        when(userAddressRepository.getByAddressOrPincodeOrCountry(anyString(), any(Pageable.class))).thenReturn(userAddressPage);
        when(userAddressPage.getContent()).thenReturn(new ArrayList<UserAddress>(){{add(new UserAddress(){{setAddressType(ADDRESS_TYPE_BOTH); setAddress(TEST);}});}});
        ResponseEntity responseEntity = userAddressServiceImpl.getUserAdress(new UserAddressRequest() {{setLimit(LIMIT); setAddressType(ADDRESS_TYPE_BOTH); setTextInput(TEST);}});
        assertTrue(((ResponseBody)responseEntity.getBody()).getAddressList().get(ADDRESS_TYPE_BOTH).getAddressType().contentEquals(BOTH));
        assertTrue(((ResponseBody)responseEntity.getBody()).getAddressList().get(ADDRESS_TYPE_BOTH).getAddress().contentEquals(TEST));
    }

    private UserAddressRequest createUserAddressRequest() {
        UserAddressRequest userAddressRequest = new UserAddressRequest();
        userAddressRequest.setTextInput("");
        userAddressRequest.setAddressType(ADDRESS_TYPE_BOTH);
        userAddressRequest.setPage(0);
        userAddressRequest.setLimit(LIMIT);
        return userAddressRequest;
    }
}
