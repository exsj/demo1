package com.home.demo.test.controller;

import com.home.demo.controller.UserAddressController;
import com.home.demo.dto.ResponseBody;
import com.home.demo.dto.UserAddressDto;
import com.home.demo.dto.UserAddressRequest;
import com.home.demo.service.UserAddressService;
import com.home.demo.utils.JsonUtil;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(UserAddressController.class)
class UserAddressControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private UserAddressService userAddressService;

    @Test
    void getUserAddress_notFound() throws Exception {
        when(userAddressService.getUserAdress(createUserAddressRequest())).thenReturn(null);
        mockMvc.perform(post("/dummy").contentType(MediaType.APPLICATION_JSON).content(JsonUtil.toJson(new UserAddressRequest()))).andExpect(status().isNotFound());
    }

    @Test
    void getUserAddress_badInput() throws Exception {
        when(userAddressService.getUserAdress(createUserAddressRequest())).thenReturn(null);
        mockMvc.perform(post("/").contentType(MediaType.APPLICATION_JSON).content("asdfasdfasdfas"))
                .andDo(print())
                .andExpect(status().isBadRequest());
    }

    @Test
    void getUserAddress_Found() throws Exception {
        //TODO check isOk why not isBadRequest for empty json
        when(userAddressService.getUserAdress(createUserAddressRequest())).thenReturn(new ResponseEntity<>(createResponseBody(), HttpStatus.OK));
        mockMvc.perform(post("/").contentType(MediaType.APPLICATION_JSON).content("{}"))
                .andDo(print())
                .andExpect(status().isOk());
    }

    private UserAddressRequest createUserAddressRequest() {
        UserAddressRequest userAddressRequest = new UserAddressRequest();
        userAddressRequest.setTextInput("uchar");
        userAddressRequest.setAddressType(0); //both
        userAddressRequest.setPage(0);
        userAddressRequest.setLimit(10);
        return userAddressRequest;
    }

    private UserAddressDto createAddressDto() {
        UserAddressDto userAddressDto = new UserAddressDto();
        userAddressDto.setSerialNo(1L);
        userAddressDto.setAddressId(1L);
        userAddressDto.setAddress("123, Bacau, Romania");
        userAddressDto.setPincode("123456");
        userAddressDto.setCountry("Romania");
        userAddressDto.setCity("Bacau");
        userAddressDto.setState("Bacau");
        userAddressDto.setAddressType("Both");
        return userAddressDto;
    }


    private ResponseBody createResponseBody(){
        ResponseBody responseBody = new ResponseBody();
        responseBody.setAddressList(new ArrayList<UserAddressDto>(){{add(createAddressDto());}});
        responseBody.setTotalRecords(1L);
        return responseBody;
    }

}