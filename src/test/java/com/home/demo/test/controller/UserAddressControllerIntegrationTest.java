package com.home.demo.test.controller;

import com.home.demo.Demo1TeamProjectApplication;
import com.home.demo.dto.UserAddressRequest;
import com.home.demo.entity.UserAddress;
import com.home.demo.repository.UserAddressRepository;
import com.home.demo.utils.JsonUtil;

import org.hamcrest.Matchers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.greaterThanOrEqualTo;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = Demo1TeamProjectApplication.class)
@AutoConfigureMockMvc
public class UserAddressControllerIntegrationTest {

    @Autowired
    private MockMvc mvc;

    @Autowired
    private UserAddressRepository repository;

    @Test
    public void whenValidInput_thenRetrieveAddressList() throws Exception {
        UserAddressRequest userAddressRequest = createUserAddressRequest();
        mvc.perform(post("/").contentType(MediaType.APPLICATION_JSON).content(JsonUtil.toJson(userAddressRequest)));
        List<UserAddress> found = repository.findAll();
        assertThat(found).filteredOn(item -> item.getAddress().contains("George")).hasSizeGreaterThan(0);
    }

    @Test
    public void givenAddressRequest_whenGetAddressList_thenStatus200() throws Exception {
        UserAddressRequest userAddressRequest = createUserAddressRequest();
        userAddressRequest.setTextInput("Buc");

        mvc.perform(post("/").contentType(MediaType.APPLICATION_JSON).content(JsonUtil.toJson(userAddressRequest)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.addressList", Matchers.hasSize(greaterThanOrEqualTo(1))))
                .andExpect(jsonPath("$.addressList[0].address", Matchers.is("str. George Cosbuc")))
                .andExpect(jsonPath("$.addressList[0].country", Matchers.is("Romania")))
                .andExpect(jsonPath("$.addressList[0].addressType", Matchers.is("Primary")))
                .andExpect(jsonPath("$.addressList[0].city", Matchers.is("Bucuresti")))
                .andExpect(jsonPath("$.addressList[0].state", Matchers.is("SECTOR 5")))
                .andExpect(jsonPath("$.addressList[0].pincode", Matchers.is("0014235")));
    }

    private UserAddressRequest createUserAddressRequest() {
        UserAddressRequest userAddressRequest = new UserAddressRequest();
        userAddressRequest.setTextInput("Buc");
        userAddressRequest.setAddressType(0); //both
        userAddressRequest.setPage(0);
        userAddressRequest.setLimit(10);
        return userAddressRequest;
    }
}

