package com.home.demo.utils;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.home.demo.dto.ResponseBody;

public class Utils {
	
	private Utils() {
		
	}
	
	public static ResponseEntity<ResponseBody> makeStatus(ResponseBody responseBody, HttpStatus status) {
		return new ResponseEntity<>(responseBody, status);
	}

}
