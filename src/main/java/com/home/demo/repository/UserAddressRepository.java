package com.home.demo.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.home.demo.entity.UserAddress;

@Repository
public interface UserAddressRepository extends JpaRepository<UserAddress, Long> {
	
	Page<UserAddress> findByAddressType(int addressType, Pageable pageable);
	Long countByAddressType(int addressType);
	
	@Query("from UserAddress u where u.address LIKE %:textInput% OR u.pincode LIKE %:textInput% OR u.country LIKE %:textInput%")
	Page<UserAddress> getByAddressOrPincodeOrCountry(@Param("textInput") String textInput, Pageable pageable);
	
	@Query("SELECT COUNT(u) from UserAddress u where u.address LIKE %:textInput% OR u.pincode LIKE %:textInput% OR u.country LIKE %:textInput%")
	Long getCountByAddressOrPincodeOrCountry(@Param("textInput") String textInput);
	
	@Query("from UserAddress u where u.addressType = :addressType AND (u.address LIKE %:textInput% OR u.pincode LIKE %:textInput% OR u.country LIKE %:textInput%)")
	Page<UserAddress> getByAddressOrPincodeOrCountryAndAddressType(@Param("textInput") String textInput, @Param("addressType") int addressType, Pageable pageable);
	
	@Query("SELECT COUNT(u) from UserAddress u where u.addressType = :addressType AND (u.address LIKE %:textInput% OR u.pincode LIKE %:textInput% OR u.country LIKE %:textInput%)")
	Long getCountByAddressOrPincodeOrCountryAndAddressType(@Param("textInput") String textInput, @Param("addressType") int addressType);
	

}
