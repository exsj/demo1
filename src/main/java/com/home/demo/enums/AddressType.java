package com.home.demo.enums;


public enum AddressType implements EnumType {
	
	BOTH(0, "Both"),
	PRIMARY(1, "Primary"), 
	SECONDARY(2, "Secondary") ;

	private final int id;
	private final String name;
	
	AddressType(Integer id, String name) {
		this.id = id;
		this.name = name;
	}

    @Override
	public Integer getId() {
		return this.id;
	}

    @Override
	public String getName() {
		return this.name;
	}
    
	public static AddressType getFromId(int id) {
		for (AddressType status : values()) {
			if (status.getId() == id) {
				return status;
			}
		}
		return null;
	}
	
	public static AddressType getFromValue(String value) {
		for (AddressType status : values()) {
			if (status.getName().equalsIgnoreCase(value)) {
				return status;
			}
		}
		return null;
	}

}
