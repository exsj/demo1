package com.home.demo.enums;

import java.io.Serializable;

public interface EnumType extends Serializable {
    Integer getId();

    String getName();
}
