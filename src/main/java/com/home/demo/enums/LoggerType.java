package com.home.demo.enums;

public enum LoggerType implements EnumType{

    INFO(1, "Info"),
    DEBUG(2, "Debug"),
    ERROR(3, "Error"),
    EXCEPTION(4, "Exception");

    private final int id;
    private final String name;

    /**
     * code
     * @param id key of LoggerType.
     * @param name value of LoggerType.
     */
    LoggerType(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    @Override
    public Integer getId() {
        return this.id;
    }

    @Override
    public String getName() {
        return this.name;
    }

    /**
     * This methods is used to fetch Enum base on given id.
     * @param id enum key
     * @return LoggerType enum
     */
    public static LoggerType getFromId(int id) {
        for (LoggerType status : values()) {
            if (status.getId() == id) {
                return status;
            }
        }
        return null;
    }
}
