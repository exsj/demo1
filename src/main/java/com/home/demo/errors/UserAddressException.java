package com.home.demo.errors;

public class UserAddressException extends IllegalArgumentException {
    public UserAddressException(String message){
        super(message);
    }
}
