package com.home.demo.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import com.home.demo.constants.Constants;
import com.home.demo.errors.UserAddressException;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.home.demo.dto.ResponseBody;
import com.home.demo.dto.UserAddressDto;
import com.home.demo.dto.UserAddressRequest;
import com.home.demo.entity.UserAddress;
import com.home.demo.enums.AddressType;
import com.home.demo.repository.UserAddressRepository;
import com.home.demo.service.UserAddressService;
import com.home.demo.utils.Utils;

@Service
public class UserAddressServiceImpl implements UserAddressService {

    private static final Logger logger = LogManager.getLogger(UserAddressServiceImpl.class);

    @Autowired
    UserAddressRepository userAddressRepository;

    @Override
    public ResponseEntity<ResponseBody> getUserAdress(UserAddressRequest userAddressRequest) throws UserAddressException{

        validateInput(userAddressRequest);

        int page = userAddressRequest.getPage();
        int limit = userAddressRequest.getLimit();
        Pageable pageable = PageRequest.of(page, limit);
        boolean addressRequestIsBoth = userAddressRequest.getAddressType() == null || userAddressRequest.getAddressType() == 0;

        if (addressRequestIsBoth) {
            return Utils.makeStatus(requestBothUserAddress(pageable, page, limit, userAddressRequest), HttpStatus.OK);
        }
		return Utils.makeStatus(requestPrimaryOrSecondaryUserAddress(pageable, page, limit, userAddressRequest), HttpStatus.OK);

    }

    private void mapUserAddressToUserAddressDto(int limit, int offset, List<UserAddressDto> addressListDto,
                                                List<UserAddress> listOfUserAddress) {
        listOfUserAddress.forEach(element -> {
            UserAddressDto userAddressDto = new UserAddressDto();
            userAddressDto.setSerialNo(listOfUserAddress.indexOf(element) + 1l + offset * limit);
            userAddressDto.setAddressId(element.getId());
            userAddressDto.setAddress(element.getAddress());
            userAddressDto.setPincode(element.getPincode());
            userAddressDto.setCountry(element.getCountry());
            userAddressDto.setCity(element.getCity());
            userAddressDto.setState(element.getState());
            userAddressDto.setAddressType(AddressType.getFromId(element.getAddressType()).getName());

            addressListDto.add(userAddressDto);
        });
    }

    private ResponseBody requestBothUserAddress(Pageable pageable, int page, int limit, UserAddressRequest userAddressRequest) {
        List<UserAddress> listOfUserAddress;
        Long totalRecords;
        List<UserAddressDto> addressListDto = new ArrayList<>();
        ResponseBody responseBody = new ResponseBody();

        if (StringUtils.isBlank(userAddressRequest.getTextInput())) {
            logger.info("Requesting both user addresses, no input");
            listOfUserAddress = userAddressRepository.findAll(pageable).getContent();
            totalRecords = userAddressRepository.count();
        } else {
            logger.info("Requesting both user addresses, with input");
            listOfUserAddress = userAddressRepository.getByAddressOrPincodeOrCountry(userAddressRequest.getTextInput(), pageable).getContent();
            totalRecords = userAddressRepository.getCountByAddressOrPincodeOrCountry(userAddressRequest.getTextInput());
        }

        mapUserAddressToUserAddressDto(page, limit, addressListDto, listOfUserAddress);
        responseBody.setTotalRecords(totalRecords);
        responseBody.setAddressList(addressListDto);
        if(responseBody.getTotalRecords() == 0) {
            logger.warn("No records corresponding to input");
        }
        return responseBody;
    }

    private ResponseBody requestPrimaryOrSecondaryUserAddress(Pageable pageable, int page, int limit, UserAddressRequest userAddressRequest) {
        List<UserAddress> listOfUserAddress;
        Long totalRecords;
        List<UserAddressDto> addressListDto = new ArrayList<>();
        ResponseBody responseBody = new ResponseBody();

        if (StringUtils.isBlank(userAddressRequest.getTextInput())) {
            logger.log(Level.INFO, "Requesting user addresses type, no input");
            listOfUserAddress = userAddressRepository.findByAddressType(userAddressRequest.getAddressType(), pageable).getContent();
            totalRecords = userAddressRepository.countByAddressType(userAddressRequest.getAddressType());
        } else {
            logger.log(Level.INFO, "Requesting user addresses type, with input");
            listOfUserAddress = userAddressRepository.getByAddressOrPincodeOrCountryAndAddressType(userAddressRequest.getTextInput(), userAddressRequest.getAddressType(), pageable).getContent();
            totalRecords = userAddressRepository.getCountByAddressOrPincodeOrCountryAndAddressType(userAddressRequest.getTextInput(), userAddressRequest.getAddressType());
        }

        mapUserAddressToUserAddressDto(page, limit, addressListDto, listOfUserAddress);

        responseBody.setTotalRecords(totalRecords);
        responseBody.setAddressList(addressListDto);
        return responseBody;
    }

    private void validateInput(UserAddressRequest userAddressRequest) {
        //validate input
//        boolean validInput = Pattern.compile(Constants.TEXT)
//                .matcher(userAddressRequest.getTextInput())
//                .matches();
//        if (!validInput) throw new UserAddressException(userAddressRequest.getTextInput() + " is not a valid input!");

        //validate addressType
        boolean validAddressType = userAddressRequest.getAddressType() == null || userAddressRequest.getAddressType() >= 0 && userAddressRequest.getAddressType() < 3;
        if (!validAddressType) throw new UserAddressException(userAddressRequest.getAddressType() + " is not a valid address type! 0 - Both; 1 - Primary; 2 - Secondary");

        //validate limit
        boolean validPageSize = userAddressRequest.getLimit() > 0 && userAddressRequest.getLimit() <= 99;
        if (!validPageSize) throw new UserAddressException(userAddressRequest.getLimit() + " is not a valid page size!");


        //validate page number
        boolean validPageNumber = userAddressRequest.getPage() >= 0 && userAddressRequest.getPage() <= 99;
        if (!validPageNumber) throw new UserAddressException(userAddressRequest.getPage() + " is not a valid page number!");


    }
}
