package com.home.demo.service;

import org.springframework.http.ResponseEntity;

import com.home.demo.dto.ResponseBody;
import com.home.demo.dto.UserAddressRequest;

public interface UserAddressService {

	ResponseEntity<ResponseBody> getUserAdress(UserAddressRequest userAddressRequest);

}
