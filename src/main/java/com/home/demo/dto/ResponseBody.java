package com.home.demo.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_EMPTY)
public class ResponseBody {
	
	private Long totalRecords;
	private List<UserAddressDto> addressList;
	private String errorMessage;

	public List<UserAddressDto> getAddressList() {
		return addressList;
	}
	public void setAddressList(List<UserAddressDto> addressList) {
		this.addressList = addressList;
	}
	public Long getTotalRecords() {
		return totalRecords;
	}
	public void setTotalRecords(Long totalRecords) {
		this.totalRecords = totalRecords;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	protected void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

}
