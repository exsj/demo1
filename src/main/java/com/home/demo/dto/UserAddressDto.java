package com.home.demo.dto;

import java.io.Serializable;

public class UserAddressDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8590586971891404320L;

	private Long serialNo;
	private Long addressId;
	private String address;
	private String pincode;
	private String country;
	private String city;
	private String state;
	private String addressType;
	
	public Long getSerialNo() {
		return serialNo;
	}
	public void setSerialNo(Long serialNo) {
		this.serialNo = serialNo;
	}
	public Long getAddressId() {
		return addressId;
	}
	public void setAddressId(Long addressId) {
		this.addressId = addressId;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getPincode() {
		return pincode;
	}
	public void setPincode(String pincode) {
		this.pincode = pincode;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getAddressType() {
		return addressType;
	}
	public void setAddressType(String addressType) {
		this.addressType = addressType;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
}
