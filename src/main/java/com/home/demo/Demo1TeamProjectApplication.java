package com.home.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Demo1TeamProjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(Demo1TeamProjectApplication.class, args);
	}

}
