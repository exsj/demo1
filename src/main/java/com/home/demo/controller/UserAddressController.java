package com.home.demo.controller;

import com.home.demo.dto.ResponseBody;
import com.home.demo.dto.UserAddressRequest;
import com.home.demo.errors.UserAddressException;
import com.home.demo.service.UserAddressService;
import com.home.demo.utils.Utils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin
@RestController
public class UserAddressController {

	private static final Logger logger = LogManager.getLogger(UserAddressController.class);

	@Autowired
	UserAddressService userAddressService;

	@GetMapping("/getAddress/{addressType}/{textInput}")
	public ResponseEntity<ResponseBody> getUserAddress(@PathVariable("addressType") Integer addressType, @PathVariable("textInput") String textInput, @RequestParam Integer page, @RequestParam Integer limit) {
		logger.info("Got request to get user addresses");
		try {
			UserAddressRequest userAddressRequest = new UserAddressRequest();
			userAddressRequest.setAddressType(addressType);
			userAddressRequest.setTextInput(textInput);
			userAddressRequest.setLimit(limit);
			userAddressRequest.setPage(page);
			return userAddressService.getUserAdress(userAddressRequest);
		} catch (UserAddressException e){
			return Utils.makeStatus(new ResponseBody(){{setErrorMessage(e.getMessage());}}, HttpStatus.BAD_REQUEST);
		}
	}

	@GetMapping("/getAddress/{addressType}")
	public ResponseEntity<ResponseBody> getUserAddress(@PathVariable("addressType") Integer addressType, @RequestParam Integer page, @RequestParam Integer limit) {
		return getUserAddress(addressType, null, page, limit);
	}

	@GetMapping("/getAddress")
	public ResponseEntity<ResponseBody> getUserAddress(@RequestParam Integer page, @RequestParam Integer limit) {
		return getUserAddress(null, null, page, limit);
	}


	@GetMapping("/getAddressQueryParam")
	public ResponseEntity<ResponseBody> getAddressQueryParam(@RequestParam(value = "addressType", required=false)  Integer addressType, @RequestParam(value = "textInput", required=false) String textInput, @RequestParam Integer page, @RequestParam Integer limit) {
		return getUserAddress(addressType, textInput, page, limit);
	}



}
