export interface Adress {
address: string,
addressId: number,
addressType: string,
city: string,
country: string,
pincode: string,
serialNo: number,
state: string
}