export interface Info {
    textInput: string,
    addressType: number,
    page: number,
    limit: number
  }
