import { Adress } from "./adress";

export interface ResponseModel {
addressList: Adress[],
totalRecordings: number
}