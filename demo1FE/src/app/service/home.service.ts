import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Info } from '../utils/info';

@Injectable({
  providedIn: 'root'
})
export class HomeService {
  baseUrl = "http://localhost:8081/demo1/getAddress/";

  constructor(private httpClient: HttpClient) {}

  sendInfo(info: Info) {
    // if(info.textInput == null || info.addressType == null){
    //   this.baseUrl = this.baseUrl + ((info.textInput) ? `${info.addressType}/${info.textInput}` : `${info.addressType}`);
    // }
    // this.baseUrl = this.baseUrl + `${info.addressType}/${info.textInput}`;
    // console.log(info.textInput);
    // console.log(this.baseUrl);
    // console.log(info);
    return this.httpClient.get(
      this.baseUrl + `${info.addressType}/${info.textInput}`,
      {
        params: {
          page: info.page.toString(),
          limit: info.limit.toString()
        }
      }
    );
  }
}
